# voxl-utils

This repository is used to generate an IPK package of ModalAI common on-target utilities supporting setup, configuration and testing tasks.

### Installation

VOXL Utilities is included in the [VOXL Software Bundle](https://docs.modalai.com/install-software-bundles/).

### Usage

Usage instructions available at the [ModalAI Technical Documentation](https://docs.modalai.com/voxl-utils/) page.

### Build Instructions
To generate the IPK from source  perform the following:

```bash
git clone git@gitlab.com:voxl-public/voxl-utils.git
cd voxl-utils
./make_package.sh ipk
```


To generate the DEB from source (for Ubuntu) , perform the following:

```bash
git clone git@gitlab.com:voxl-public/voxl-utils.git
cd voxl-utils
./make_deb_package.sh deb
```

## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
voxl-utils$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.


```bash
(outside of docker)
voxl-utils$ ./deploy_to_voxl.sh ssh 192.168.1.123
```