#!/bin/bash

# This file will be overwritten whenever voxl-utils is updated,
# Any customizations to your environment should be made in ~/.bashrc

# ModalAI bash tweaks 

# ubuntu ssh sets TERM to xterm-256color which screws with ROS
# change it to linux spec
export TERM=linux

shopt -s checkwinsize

resize > /dev/null

HOME="/home/root"

# Can manually set the prompt name if desired
if [ -f /usr/share/platform_name ] ; then

    PLATFORM_NAME="$(cat /usr/share/platform_name)"

else
    case $(voxl-platform) in
        M0104|M0204)
            PLATFORM_NAME="voxl2-mini"
            ;;

        M0054)
            PLATFORM_NAME="voxl2"
            ;;

        M0052)
            PLATFORM_NAME="RB5"
            ;;

        VOXL)
            PLATFORM_NAME="voxl"
            ;;

        *)
            PLATFORM_NAME="unknown platform"
            ;;
    esac
fi

# let user know they are onboard
PS1=""
# Platform Name
PS1+="${RAW_SET_BOLD}${RAW_CLR_LIT_RED}${PLATFORM_NAME}${RAW_RESET_ALL}"
# User can set EN_PS1_IP to true to have the last two parts of IP in WLAN0 show up in green
PS1+="${RAW_CLR_LIT_GRN}\$([ \$EN_PS1_IP ] && localhost --ps1)${RAW_RESET_ALL}:"
# Working Directory
PS1+="${RAW_SET_BOLD}${RAW_CLR_BLU}\w${RAW_RESET_ALL}"
# Git branch if we're in a
PS1+="${RAW_CLR_LIT_YLW}\$(__git_ps1 '(%s)')${RAW_RESET_ALL}\$ "

ECHO_PS1_NAME="${SET_BOLD}${CLR_LIT_RED}${PLATFORM_NAME}${RESET_ALL}:"

# Tab completion function that any processes looking for pipes as arguments
#  should use. Will set COMPREPLY to contain a list of pipes whose info files
#  contain any one of the strings provided as parameters to this function.
#
# Sample Usage: 
#  _voxl_inspect_vibration(){
#      _voxl_tab_complete "imu_data_t"
#  }

_voxl_tab_complete(){

    local comps=""
    
    for file in /run/mpa/*
    do
    	local longFile="$file/info"
    	if [ -f  $longFile ] || [ -p $longFile ]; then
    		for string in $@
    		do
	    		if egrep -iq $string $longFile ; then
			    	comps="${comps} $(basename $file)"
			    	continue 2
			    fi
			done
	    fi
    done

    COMPREPLY=( $(compgen -W '$comps' -- ${COMP_WORDS[COMP_CWORD]}) )
    return 0
}

_voxl_tab_complete_pipes_with_available_control(){

    local comps=""
    
    for file in /run/mpa/*
    do
        local controlFile="$file/control"
        if [ -p $controlFile ]; then
            comps="${comps} $(basename $file)"
        fi
    done

    COMPREPLY=( $(compgen -W '$comps' -- ${COMP_WORDS[COMP_CWORD]}) )
    return 0
}

_voxl_tab_complete_control_commands(){

    local file="/run/mpa/${1}/info"
    if [ -f  $file ] || [ -p $file ]; then
        local comps=$(cat /run/mpa/${1}/info | grep "available_commands" | sed  's-"available_commands":--g' | egrep -o "\"[^,]*\"" | egrep -o "[^\"]*")

        COMPREPLY=( $(compgen -W '$comps' -- ${COMP_WORDS[COMP_CWORD]}) )
        return 0
    fi

    local file="${1}/info"
    if [ -f  $file ] || [ -p $file ]; then
        local comps=$(cat ${1}/info | grep "available_commands" | sed  's-"available_commands":--g' | egrep -o "\"[^,]*\"" | egrep -o "[^\"]*")

        COMPREPLY=( $(compgen -W '$comps' -- ${COMP_WORDS[COMP_CWORD]}) )
        return 0
    fi
}

topgrep(){
    top -b -n 1 | egrep "${1}|COMMAND" | grep -v "egrep"
}

# OpenMP projects complain if this isnt set
export TSAN_OPTIONS="ignore_noninstrumented_modules=1"

alias ls="ls --color"
alias neofetch="neofetch --ascii /usr/share/modalai/voxl-utils/neofetch/neofetch_logo.txt --ascii_colors 2"

LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;1:ow=34;1:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.ipk=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:'
export LS_COLORS
