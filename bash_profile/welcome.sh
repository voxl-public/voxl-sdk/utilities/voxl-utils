#!/bin/bash

. ~/.profile.d/modal_bash_formatting.sh

echo -e ${DISABLE_WRAP}

if date | grep -q " Oct 31 " ; then
    # HALLOWEEEEEEEENNNNNN
    echo -e ""
    echo -e "  ███▄ ▄███▓ ▒█████  ▓█████▄  ▄▄▄       ██▓       ${CLR_GRN} ▄▄▄       ██▓${CLR_RESET}"
    echo -e " ▓██▒▀█▀ ██▒▒██▒  ██▒▒██▀ ██▌▒████▄    ▓██▒       ${CLR_GRN}▒████▄    ▓██▒${CLR_RESET}"
    echo -e " ▓██    ▓██░▒██░  ██▒░██   █▌▒██  ▀█▄  ▒██░       ${CLR_GRN}▒██  ▀█▄  ▒██▒${CLR_RESET}"
    echo -e " ▒██    ▒██ ▒██   ██░░▓█▄   ▌░██▄▄▄▄██ ▒██░       ${CLR_GRN}░██▄▄▄▄██ ░██░${CLR_RESET}"
    echo -e " ▒██▒   ░██▒░ ████▓▒░░▒████▓  ▓█   ▓██▒░██████▒   ${CLR_GRN} ▓█   ▓██▒░██░${CLR_RESET}"
    echo -e " ░ ▒░   ░  ░░ ▒░▒░▒░  ▒▒▓  ▒  ▒▒   ▓▒█░░ ▒░▓  ░   ${CLR_GRN} ▒▒   ▓▒█░░▓  ${CLR_RESET}"
    echo -e " ░  ░      ░  ░ ▒ ▒░  ░ ▒  ▒   ▒   ▒▒ ░░ ░ ▒  ░   ${CLR_GRN}  ▒   ▒▒ ░ ▒ ░${CLR_RESET}"
    echo -e " ░      ░   ░ ░ ░ ▒   ░ ░  ░   ░   ▒     ░ ░      ${CLR_GRN}  ░   ▒    ▒ ░${CLR_RESET}"
    echo -e "        ░       ░ ░     ░          ░  ░    ░  ░   ${CLR_GRN}      ░  ░ ░  ${CLR_RESET}"
    echo -e "                      ░                           ${CLR_GRN}              ${CLR_RESET}"
    echo -e ""
else
    echo -e "                                              ${CLR_GRN}         ${CLR_GRN}  ▂▂▂▂▂▂▂▂▂▂▂▂▂${CLR_GRN}            ${CLR_RESET}"
    echo -e "                                              ${CLR_GRN}      ▂▄▆${CLR_GRN}▆██▛▀▀▀▀▀▀▀▀▜██${CLR_GRN}██▆▆▄▂      ${CLR_RESET}"
    echo -e " ███╗   ███╗ ██████╗ ██████╗  █████╗ ██╗      ${CLR_GRN}   ▗▆████${CLR_GRN}▀▔             ${CLR_GRN}▔▔▀▀▀▀▚▄    ${CLR_RESET}"
    echo -e " ████╗ ████║██╔═══██╗██╔══██╗██╔══██╗██║      ${CLR_GRN} ▗▟████▀ ${CLR_WHT}    ▗██▖    ▐█ ${CLR_GRN}  ▝▀▆▄▄▄    ${CLR_RESET}"
    echo -e " ██╔████╔██║██║   ██║██║  ██║███████║██║      ${CLR_GRN}▟████▀   ${CLR_WHT}   ▗█▘▝█▖   ▐█ ${CLR_GRN}      ▜█▀█▄ ${CLR_RESET}"
    echo -e " ██║╚██╔╝██║██║   ██║██║  ██║██╔══██║██║      ${CLR_GRN}█▌ ▐█▌   ${CLR_WHT}  ▗█▘  ▝█▖  ▐█ ${CLR_GRN}      ▐▄ ▄█ ${CLR_RESET}"
    echo -e " ██║ ╚═╝ ██║╚██████╔╝██████╔╝██║  ██║███████╗ ${CLR_GRN} ▀████   ${CLR_WHT} ▗█▘    ▝█▖ ▐█ ${CLR_GRN}    ▂▄███▀  ${CLR_RESET}"
    echo -e " ╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝ ${CLR_GRN}   ▀▀██▄▄${CLR_GRN}               ${CLR_GRN} ▂▆███▀     ${CLR_RESET}"
    echo -e "                                              ${CLR_GRN}       ▀▀${CLR_GRN}██▄▄ ▀▀▆▄▄▄▄▆██${CLR_GRN}▀▀▀▘        ${CLR_RESET}"
fi

echo
echo
voxl-version -q


IP=$(voxl-my-ip)
if [ $? -ne 0 ]; then
    echo "no current network connection"
else
    echo "current IP:   $IP"
fi
echo -e "--------------------------------------------------------------------------------"
echo
echo -e ${ENABLE_WRAP}
